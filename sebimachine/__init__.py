#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
"""
Sebi-Machine.
"""

__author__ = "Dusty.P"
__contributors__ = (__author__, "Neko404NotFound", "Dusty.P", "davfsa", "YashKandalkar")

__license__ = "MIT"
__title__ = "Dev-Sebi"
__version__ = "0.0.1"

__repository__ = f"https://gitlab.com/{__author__}/{__title__}.git"
__url__ = __repository__
